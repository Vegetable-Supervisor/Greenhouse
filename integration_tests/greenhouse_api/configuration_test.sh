# test of the /configuration route of the GreenHouse https server

RED='\033[0;31m'
NC='\033[0m'
DEBUG="false"

# variables
json_configuration='{
  "name": "3232",
  "description": "a desc",
  "modules": {}
}'

# helpers
fail() {
    echo -e "${RED}*** Failed test $1 ***${NC}"
      exit 1
}

setup() {
  python3 main.py >&- 2>&- &
}

cleanup() {
  pkill python3 >&- 2>&- &
}

check_json() {
  if jq -e . >/dev/null 2>&1 <<<"$1"; then
    :
  else
    echo "invalid json: $1"
    exit 1
  fi
}

# functions
post_configuration() {
  post=$(curl -sSf --request POST --data "$1" -H "Content-Type: application/json" -k "https://localhost:5000/configuration")
}

get_configuration() {
  get=$(curl -sSf --request GET -k "https://localhost:5000/configuration")
}

# test

setup
sleep 2
# update the configuration of the freshly created greenhouse
post_configuration "$json_configuration"
# retrieve updated configuration
get_configuration
cleanup

# check that responses are correct json
check_json $post
check_json $get

# check that configuration has been updated
if [ "$post" != "$get" ]; then
  fail "configurations do not match: $post, $get"
fi

# pass
exit 0
