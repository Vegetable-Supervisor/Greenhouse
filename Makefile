PROJECT_NAME := "GreenHouse"
PKG := "gitlab.com/Vegetable-Supervisor/$(PROJECT_NAME)"

.PHONY: all dep build clean test coverage coverhtml lint

all: build

build: install ## compile python
	python3 -m compileall .

install: ## install dependencies
	@python3 -m pip install -r requirements.txt	

lint: ## lint files
	@python3 -m pylint app

test: install ## unit test
	@(cd app/; python3 -m pytest;)

coverage: ## unit test coverage
	@(cd app/; python3 -m pytest --cov .;)

integration: ## perform integration tests
	@run-parts ./integration_tests/greenhouse_api

clean: ## delete pyc files
	@find . -name "*.pyc" -delete
	@find . -name "__pycache__" -exec rm -rv {} + 
	@find . -name ".coverage" -exec rm -rv {} + 
	@find . -name ".pytest_cache" -exec rm -rv {} + 

db_clean: ## delete database files
	@find . -name "*.sqlite" -delete

help: ## print this help
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
