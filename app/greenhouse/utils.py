"""Utility functions."""

from greenhouse.greenhouse import GreenHouse
import asyncio
import threading
from lib.event_ts import EventTs

def run_core(gh: GreenHouse):
    """Run the core run-loop in the background."""
    # run core run loop in a separate thread
    loop = asyncio.get_event_loop()
    stop_event = EventTs()
    t = threading.Thread(target=gh.run, args=(loop, stop_event))
    t.start()
    return stop_event

def stop_core(gh: GreenHouse):
    """Stop the core run-loop."""
    loop = asyncio.get_event_loop()
    loop.stop()

