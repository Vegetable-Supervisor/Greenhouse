import unittest
import os

from greenhouse.greenhouse import GreenHouse
from greenhouse.configuration import Configuration


class GreenHouseTest(unittest.TestCase):
    """GreenHouse class tests."""

    tt = [
        {"database_file": "db1.test_db",
            "name": "Configuration('test greenhouse 1', 'desc1', {})",
            "configuration": Configuration('test greenhouse 1', 'desc1', {})},
        {"database_file": "db2.test_db",
            "name": "Configuration('', 'desc2', {})",
            "configuration": Configuration('', 'desc2', {})},
        {"database_file": "db3.test_db",
            "name": "Configuration('test greenhouse 3', '', {})",
            "configuration": Configuration('test greenhouse 3', '', {})},
    ]

    database_file = "db.test_db"

    def tearDown(self):
        # delete all database files
        items = os.listdir(".")
        for item in items:
            if item.endswith(".test_db"):
                os.remove(os.path.join(".", item))

    def test_name(self):
        """Test the correctness of the 'name' attribute after an update."""
        for tc in GreenHouseTest.tt:
            gh = GreenHouse(database_file=GreenHouseTest.database_file)
            gh._GreenHouse__name = tc["configuration"].name
            self.assertEqual(tc["configuration"].name, gh.name, "{} expected name: {}, got: {}".format(
                tc["name"], tc["configuration"].name, gh.name))

    def test_identifier_unique_in_two(self):
        """Test the uniqueness of the 'uuid' attribute for two 'identic' GreenHouses."""
        gh1 = GreenHouse(database_file="1" + GreenHouseTest.database_file)
        gh2 = GreenHouse(database_file="2" + GreenHouseTest.database_file)
        self.assertNotEqual(gh1._GreenHouse__uuid, gh2._GreenHouse__uuid,
                            "two greenhouses should not have same uuid: {}".format(gh1._GreenHouse__uuid))

    def test_incomplete_update(self):
        """Test the 'update' method of the GreenHouse's configuration when configuration is incomplete."""
        complete_conf = Configuration("gh1", "desc1", {})
        incomplete_conf = Configuration(description="desc2")

        gh = GreenHouse(database_file=GreenHouseTest.database_file)
        gh.update(complete_conf)
        gh.update(incomplete_conf)

        self.assertEqual(gh.name, complete_conf.name)

    def test_update(self):
        """Test the 'update' method of the GreenHouse's configuration."""
        gh = GreenHouse(database_file="db.test_db")
        for tc in GreenHouseTest.tt:
            gh.update(tc["configuration"])
            self.assertEqual(gh.configuration, tc["configuration"], "after an update, configuration of greenhouse should be updated: expected: {}, got: {}".format(
                tc["configuration"], gh.configuration))

    def test_force_update(self):
        """Test the '__force_update' method of GreenHouse after being updated a first time."""
        new_conf = Configuration("new greenhouse", "new description", {})

        for tc in GreenHouseTest.tt:
            gh = GreenHouse(database_file=tc["database_file"])
            gh.update(tc["configuration"])
            gh._GreenHouse__force_update(new_conf)
            self.assertEqual(gh.name, new_conf.name, "after a force update, name of the greenhouse should match the configuration's: expected {}, got {}".format(
                new_conf.name, gh.name))

    def test_force_update_old_empty(self):
        """Test the '__force_update' method of GreenHouse when not configured once.
        Then '__force_update' should perform the same as 'update'
        """
        new_conf = Configuration("new greenhouse", "new description", {})

        for tc in GreenHouseTest.tt:
            gh1 = GreenHouse(database_file="1" + tc["database_file"])
            gh2 = GreenHouse(database_file="2" + tc["database_file"])
            gh1.update(new_conf)
            gh2._GreenHouse__force_update(new_conf)
            self.assertEqual(gh1.configuration, gh2.configuration, "when not configured, 'update' and '__force_update' a GreenHouse's configuration should perform the same: expected {}, got {}".format(
                gh1.configuration, gh2.configuration))

    def test_force_update_new_empty(self):
        """Test the '__force_update' method of GreenHouse with a new empty configuration.
        Then '__force_update' should update the configuration to be 'empty'.
        """
        empty_conf = Configuration()

        for tc in GreenHouseTest.tt:
            gh = GreenHouse(database_file=GreenHouseTest.database_file)
            gh.update(tc["configuration"])
            gh._GreenHouse__force_update(empty_conf)
            self.assertEqual(gh.configuration, empty_conf, "after a '__force_update' with an empty configuration, greenhouse's configuration should be empty: expected: {}, got: {}".format(
                empty_conf, gh.configuration))

    def test_update_bad_configuration_object(self):
        """Test 'update' method when passed a bad object."""
        gh = GreenHouse(database_file=GreenHouseTest.database_file)
        with self.assertRaises(AssertionError):
            gh.update(object())

    def test_force_update_bad_configuration_object(self):
        """Test '__force_update' method when passed a bad object."""
        gh = GreenHouse(database_file=GreenHouseTest.database_file)
        with self.assertRaises(AssertionError):
            gh._GreenHouse__force_update(object())

if __name__ == "__main__":
    unittest.main()
