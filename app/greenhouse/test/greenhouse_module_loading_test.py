import unittest

from greenhouse.greenhouse import GreenHouse
from greenhouse.logical_modules.printer import Printer as PrinterModule
from greenhouse.logical_modules.mock_module import MockModule
from greenhouse.configuration import Configuration
from main import run_core

class GreenHouseModuleLoadingTest(unittest.TestCase):
    """GreenHouse Module Loading/Unloading tests."""

    def setUp(self):
        self.gh = GreenHouse(restart=True)
        self.core_stopper = run_core(self.gh)

    def tearDown(self):
        self.core_stopper.set()

    def test_loading_module(self):
        """Try to load a module and unload it.

        Module is expected to be run correctly, and unloaded successfully.
        """
        module_name = "test_printer"
        self.gh.load_module(module_name=module_name, module_class=PrinterModule)
        self.assertTrue(self.gh._is_module_running(module_name=module_name))
        self.gh.unload_module(module_name=module_name)
        self.assertFalse(self.gh._is_module_running(module_name=module_name))

    def test_loading_module_invalid_configuration(self):
        """Try to load a module with an incorrect configuration."""
        module_name = "test_printer"
        with self.assertRaises(ValueError):
            self.gh.load_module(module_name=module_name, module_class=PrinterModule, cfg={"hello": "there"})

    def test_unload_all_modules(self):
        """Try to unload all modules."""
        modules = ["m1", "m2", "m3"]
        for m in modules:
            self.gh.load_module(module_name=m, module_class=PrinterModule)
            self.assertTrue(self.gh._is_module_running(module_name=m))

        self.gh.unload_all_modules()

        for m in modules:
            self.assertFalse(self.gh._is_module_running(m))

    def test_loading_bad_configuration(self):
        """Try to load a module with a wrong configuration.

        Should raise a ValueError.
        """
        module_name = "test_module"
        cfg = {"answer": 0}
        with self.assertRaises(ValueError):
            self.gh.load_module(module_name=module_name, module_class=MockModule, cfg=cfg)
        self.assertFalse(self.gh._is_module_running(module_name=module_name))
