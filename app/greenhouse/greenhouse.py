"""Greenhouse."""

import asyncio
import logging
from copy import copy
from threading import RLock
from uuid import uuid1

from sqlitedict import SqliteDict

from greenhouse.configuration import Configuration
from greenhouse.logical_module import LogicalModule


class GreenHouse:
    """A GreenHouse represents a connected greenhouse."""

    __default_fields = {
        "_GreenHouse__uuid": None,
        "_GreenHouse__name": None,
        "_GreenHouse__description": None,
        "_GreenHouse__modules": None,
        "_GreenHouse__module_cfgs": None,
        "_GreenHouse__physical_modules": None,
    }

    def __getattribute__(self, name: str):
        """Overrides object.__getattribute__ to load attributes from the database."""
        if name in GreenHouse.__default_fields:
            return self.__db[name]
        return object.__getattribute__(self, name)

    def __setattr__(self, name: str, value):
        """Overrides object.__setattr__ to write attributes to the database."""
        if name in GreenHouse.__default_fields:
            self.__db[name] = value
            return
        super().__setattr__(name, value)

    def __str__(self):
        return f"Greenhouse: {self.__name}"

    def __init__(self, restart: bool = False, database_file: str = "greenhouse_db.sqlite"):
        """Create a connected GreenHouse.

        Default working values are given when constructed.

        Args:
            restart (bool): override database with default values
            database_file (str): sqlitedict databse file path
        """

        self.__lock = RLock()

        with self.__lock:
            # database
            self.__db = SqliteDict(database_file, autocommit=True)

            # initialize greenhouse from existing database
            # or loads default values
            for key, value in GreenHouse.__default_fields.items():
                if restart or key not in self.__db:
                    self.__db[key] = value

            # override default attributes if needed
            if restart or self.__uuid == GreenHouse.__default_fields["_GreenHouse__uuid"]:
                self.__uuid = uuid1()
            if restart or self.__name == GreenHouse.__default_fields["_GreenHouse__name"]:
                self.__name = f"Greenhouse_{self.__uuid}"
            if restart or self.__modules == GreenHouse.__default_fields["_GreenHouse__modules"]:
                self.__modules = set()
            if restart or self.__module_cfgs == GreenHouse.__default_fields["_GreenHouse__module_cfgs"]:
                self.__module_cfgs = {}
            if restart or self.__physical_modules == GreenHouse.__default_fields["_GreenHouse__physical_modules"]:
                self.__physical_modules = {}

            # runtime attributes
            self.__modules_by_name = {}
            self.__tasks_by_name = {}
            # event-loop
            self.__loop = None

            # configuration for the greenhouse itself, not its modules
            self.__configuration = Configuration(
                name="New GreenHouse # {}".format(self.__uuid), description="not yet configured")
            # self.update(self.__configuration)

    # GETTERS

    @property
    def identifier(self):
        """Return the identifier of the greenhouse."""
        with self.__lock:
            return str(self.__uuid)

    @property
    def name(self):
        """Return the name of the greenhouse."""
        with self.__lock:
            return str(self.__name)

    @property
    def configuration(self):
        """Return the configuration of the greenhouse."""
        with self.__lock:
            return copy(self.__configuration)

    def get_modules_names(self):
        """Return the set of names of loaded modules."""
        return set(self.__modules_by_name.keys())

    def _is_module_running(self, module_name: str):
        """Return True if the module is running."""
        with self.__lock:
            if module_name not in self.__modules_by_name.keys():
                return False
            task = self.__tasks_by_name[module_name]
            return not (task.cancelled() or task.done())

    async def main_coroutine(self, stop_event):
        """Main coroutine of the event-loop."""
        logging.debug("event-loop started")
        # TODO: avoid polling
        while not stop_event.is_set():
            await asyncio.sleep(1)
        logging.debug("event-loop stopped")

    def run(self, loop: asyncio.events.AbstractEventLoop, stop_event: asyncio.Event):
        """Run the Greenhouse core run-loop.

        This is where all the modules' run coroutine are executed.

        Can be run in its own thread, e.g. :
            >>> loop = asyncio.get_event_loop()
            >>> t = threading.Thread(target=greenhouse.run, args=(loop,))

        """
        with self.__lock:
            if self.__loop is not None:
                raise RuntimeError("modules's run loop already initialized")
            self.__loop = loop
        asyncio.set_event_loop(loop)
        loop.run_until_complete(self.main_coroutine(stop_event))
        logging.debug("greenhouse core thread stopped")

    # MODULES LOADING / UNLOADING

    # TODO: this is to load a logical module, change name
    def load_module(self, module_name: str, module_class: type, cfg: dict = None):
        """Load a module onto the GreenHouse."""
        if not issubclass(module_class, LogicalModule):
            raise ValueError(
                f"cannot load module: given class {module_class.__name__} is not a module type"
            )

        with self.__lock:
            if self.__loop is None:
                raise RuntimeError(
                    "cannot load module: run-loop is not started.")

            already_exists = module_name in self.__modules_by_name.keys() \
                or module_name in self.__tasks_by_name.keys()

            if already_exists:
                raise ValueError(
                    f"cannot load module: module name already exists: {module_name}")

            # instanciate module

            module = module_class(self.__physical_modules, cfg)

            self.__modules = self.__modules | set([module_name])

            # module configuration setup
            # if cfg is specified, override saved configuration
            # otherwise load configuration from database
            if module_name not in self.__module_cfgs:
                # add configuration entry
                self.__module_cfgs = {**self.__module_cfgs, module_name: {}}
            if cfg is not None:
                # update configuration
                self.__module_cfgs = {**self.__module_cfgs, module_name: cfg}

            cfg = self.__module_cfgs[module_name]

            self.__modules_by_name[module_name] = module

            # launch module's 'run' coroutine on the run-loop
            self.__tasks_by_name[module_name] = asyncio.ensure_future(
                module.run(), loop=self.__loop)

            def callback():
                try:
                    fetch_count = fut.result()
                except Exception as e:
                    log.exception('Unexpected error: ', e)
                else:
                    print("OK")
                print("module exited")

            self.__tasks_by_name[module_name].add_done_callback(callback)

        logging.info(f"module loaded: {module_name}")

    def unload_module(self, module_name: str):
        """Unload a started module."""
        with self.__lock:
            if self.__loop is None:
                raise RuntimeError(
                    "cannot unload module: run-loop is not started.")

            module_exists = module_name in self.__modules_by_name.keys() \
                and module_name in self.__tasks_by_name.keys() \
                and module_name in self.__modules

            if not module_exists:
                raise ValueError(
                    f"cannot unload module: module is not started: {module_name}")

            # cancel task
            # request the task to be stopped
            # TODO: relies on the module to be correct
            # maybe there is a better way to do this, like a timeout
            logging.info(f"requesting module to stop: {module_name}")

            self.__tasks_by_name[module_name].cancel()

            self.__modules = self.__modules - set([module_name])
            del self.__tasks_by_name[module_name]
            del self.__modules_by_name[module_name]

            # TODO: maybe delete the configuration of the module ?

    def unload_all_modules(self):
        """Unload all started modules."""
        with self.__lock:
            logging.info("requesting all modules to stop")
            modules = list(self.__modules_by_name.keys())
            for module_name in modules:
                self.unload_module(module_name)
            logging.info("all modules stopped")

    # MODULE CONFIGURATION

    def get_module_configuration(self, module_name: str) -> dict:
        """Return the configuration of the given module, or None if does not exists.

        Args:
            module_name (str): name of the module to get the configuration from.
        """
        return self.__module_cfgs.get(module_name)

    def set_module_configuration(self, module_name: str, cfg: dict) -> None:
        """Set the configuration of the given module.

        Args:
            module_name (str): name of the module to get the configuration from.
            cfg (dict): configuration to set.
        Raises:
            KeyError: if the module does not exist.
        """
        if module_name not in self.__modules:
            raise KeyError(f"module {module_name} does not exist")
        self.__modules_by_name[module_name].update_configuration(cfg)
        self.__module_cfgs = {**self.__module_cfgs, module_name: cfg}

    # CONFIGURATION UPDATES

    def __force_update(self, configuration):
        """Erase previous configuration and reconfigure with new.

        Args:
            configuration (Configuration): new configuration for the greenhouse
        """
        self.__configuration = Configuration()
        self.update(configuration)

    def update(self, configuration):
        """Update the GreenHouse with the given configuration.

        Args:
            c (Configuration): new configuration for the greenhouse
        """
        assert(isinstance(configuration, Configuration))
        logging.info("updating configuration with: {}".format(configuration))
        try:
            with self.__lock:
                cfg = configuration
                # name update
                if cfg.name != "":
                    self.__name = cfg.name

                # description update
                if self.__description != "":
                    self.__description = cfg.description

                # modules update
                enabled_modules = set(self.__modules_by_name.keys())
                required_modules = set(cfg.modules.keys())

                add_modules_names = required_modules - enabled_modules
                del_modules_names = enabled_modules - required_modules

                # TODO module loading/unloading

                # finally set the configuration
                self.__configuration = cfg

        except Exception as e:
            # restore configuration
            self.__force_update(self.__configuration)
