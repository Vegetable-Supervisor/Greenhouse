from abc import ABC, abstractmethod
from ..physical_module import PhysicalModule


class SprayTemplate(PhysicalModule, ABC):
    """ A SprayTemplate an interface for water spraying."""

    def __init__(self):
        """Construct a new SprayTemplate."""
        super().__init__("SprayPhysicalModule")

    @property
    @abstractmethod
    def is_spraying(self) -> bool:
        """Return True if currently spraying water, False otherwise."""
        return NotImplementedError

    @abstractmethod
    def toggle_spray(self) -> ():
        """Toggle spraying of water."""
        return NotImplementedError
