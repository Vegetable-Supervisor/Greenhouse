from abc import ABC, abstractmethod
from ..physical_module import PhysicalModule


class WindowTemplate(PhysicalModule, ABC):
    """ A WindowTemplate, an interface for opening and closing a window."""

    def __init__(self):
        """Construct a new WindowTemplate."""
        super().__init__("WindowPhysicalModule")

    @abstractmethod
    def is_open(self) -> bool:
        """Return True if window is opened."""
        raise NotImplementedError

    @abstractmethod
    def open(self):
        """Open the window."""
        raise NotImplementedError

    @abstractmethod
    def close(self):
        """Close the window."""
        raise NotImplementedError