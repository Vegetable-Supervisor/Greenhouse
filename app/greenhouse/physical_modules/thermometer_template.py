from abc import ABC, abstractmethod
from ..physical_module import PhysicalModule


class ThermometerTemplate(PhysicalModule, ABC):
    """ A ThermometerTemplate, an interface for reading the temperature inside the greenhouse."""

    def __init__(self):
        """Construct a new ThermometerTemplate."""
        super().__init__("ThermometerPhysicalModule")

    @abstractmethod
    def read_temperature(self) -> float:
        """Return the temperature inside the greenhouse as celcius."""
        raise NotImplementedError