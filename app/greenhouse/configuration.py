from greenhouse.logical_module import LogicalModule

class Configuration:
    """A Configuration represents the configuration of a GreenHouse.

    Data Transfer Object.
    """
    def __init__(self, name: str = "", description: str = "", modules: dict = {}):
        """Create a GreenHouse Configuration.

        Args:
            name (str): name of the GreenHouse, should be unique, e.g. "garden greenhouse #1"
            description (str): small description of a GreenHouse, e.g. "Lettuce and Soy"
            modules (dict): map of modules name to module class, e.g. {"side window": ModuleWindow, "top window": ModuleWindow}
        """

        self.name = name
        self.description = description
        self.modules = modules.copy()

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    def __str__(self):
        return "Configuration(name={}, description={}, modules={})".format(self.name, self.description, self.modules)

def validate_configuration(json_configuration: dict):
    """Check a JSON-serialized Configuration for validity.

    Args:
        json_configuration (dict): json serialized configuration
    Returns:
        bool: True if json_configuration represents a Configuration, False otherwise
    """
    c = json_configuration

    if not (('name', 'description', 'modules') <= c.keys()):
        return False

    if not isinstance(c["name"], str):
        return False
    if not isinstance(c["description"], str):
        return False
    if not isinstance(c["modules"], dict):
        return False
    modules = c["modules"]
    for k, v in modules.items():
        if not isinstance(k, str) or not issubclass(v, LogicalModule):
            return False
    return True

def decode_configuration(json_configuration: dict):
    """Decode a JSON-serialized Configuration into a new Configuration instance.

    Args:
        json_configuration (dict): valid json serialized configuration
    Returns:
        Configuration instance created from json_configuration
    """
    assert(validate_configuration(json_configuration))

    name = json_configuration.get("name", "")
    description = json_configuration.get("description", "")
    modules = json_configuration.get("modules", {})

    return Configuration(name=name, description=description, modules=modules)
