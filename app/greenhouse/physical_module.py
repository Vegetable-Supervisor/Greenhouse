"""Greenhouse Physical Module Abstract definition."""

from abc import ABC, abstractmethod
from lib.immutable_dict import ImmutableDict


class PhysicalModule(ABC):
    """A PhysicalModule is a physical module for a greenhouse.
    It implements the control over a hardware device.

    For example, a "temperature sensor", a "openable window".
    """

    def __init__(self, name: str):
        """Construct a new PhysicalModule.

        Args:
            name (str): name of the module, unique in the greenhouse
        """

        self.name = name
