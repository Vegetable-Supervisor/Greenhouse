"""
A Mock Module for testing and documentation purposes.
The module does nothing, but can be loaded, and has a configuration.
"""

import asyncio

from ..logical_module import LogicalModule
from typing import Iterable


class MockModule(LogicalModule):
    """Simple module that does nothing."""

    __default_cfg = {
        "answer": 42,
    }

    def __init__(self, physical_modules: dict, cfg: dict = None):
        """Initializes the Mock Module module without loading it.

        Args:
            name (str): identifier of the module, unique.
            cfg (dict): configuration for the module.
        """
        if cfg is None or cfg == {}:
            cfg = MockModule.__default_cfg

        super().__init__("mock_module", physical_modules, cfg)

    def validate_configuration(self, module_cfg: dict):
        """Validates the configuration.

        Return True if and only if the configuration is legal.
        """
        return "answer" in module_cfg and module_cfg["answer"] == 42

    async def run(self):
        try:
            while True:
                await asyncio.sleep(1)
        finally:
            print("{self.name}: requested to be stopped")

    @property
    def physical_module_dependencies(self) -> Iterable[type]:
        """No physical module needed."""
        return []