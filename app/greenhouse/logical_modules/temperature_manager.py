"""
A logical module that regulates the temperature inside the greenhouse.
It uses a thermometer as well as a window that can be opened and closed.
"""

import asyncio

from ..logical_module import LogicalModule
from ..physical_modules.thermometer_template import ThermometerTemplate
from ..physical_modules.window_template import WindowTemplate
from typing import Iterable


class TemperatureManager(LogicalModule):
    """Module managing the temperature."""

    __default_cfg = {
        "opening_temperature": 25,
        "closing_temperature": 23,
        "polling_frequency": 60,  # 1 minute
    }

    def __init__(self, physical_modules: dict, cfg: dict = None):
        super().__init__("TemperatureManager", physical_modules, TemperatureManager.__default_cfg)

    @property
    def physical_module_dependencies(self) -> Iterable[type]:
        return [WindowTemplate, ThermometerTemplate]

    def validate_configuration(self, module_cfg: dict) -> bool:
        if "opening_temperature" not in module_cfg or "closing_temperature" not in module_cfg:
            return False
        return module_cfg["opening_temperature"] > module_cfg["closing_temperature"]

    async def run(self) -> None:
        try:
            while True:

                module_loaded = ThermometerTemplate.__name__ in self.physical_modules \
                    and WindowTemplate.__name__ in self.physical_modules
                if not module_loaded:
                    await asyncio.sleep(self.configuration.get("polling_frequency"))
                    continue

                thermometer = self.physical_modules[ThermometerTemplate.__name__]
                window = self.physical_modules[WindowTemplate.__name__]

                temp = thermometer.read_temperature()
                is_open = window.is_open()

                if not is_open and temp >= self.configuration.get("opening_temperature"):
                    window.open()
                elif is_open and temp <= self.configuration.get("closing_temperature"):
                    window.close()

                await asyncio.sleep(self.configuration.get("polling_frequency"))
        finally:
            print("{self.name}: requested to be stopped")
