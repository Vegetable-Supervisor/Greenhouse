"""A greenhouse module printing a message regularly."""

import asyncio
from typing import Iterable

from greenhouse.logical_module import LogicalModule


class Printer(LogicalModule):
    """Simple module that prints a message regularly."""

    __default_cfg = {
        "message": "hello world",
        "period": 1,
    }

    def __init__(self, physical_modules: dict, cfg: dict = None):
        """Initializes the Printer module without loading it.

        Args:
            name (str): identifier of the module, unique.
            cfg (dict): configuration for the module.
        """
        if cfg is None or cfg == {}:
            cfg = Printer.__default_cfg

        super().__init__("printer", physical_modules, cfg)

    def validate_configuration(self, module_cfg: dict):
        """Validates the configuration.

        Return True if and only if and only if the configuration is legal.
        """
        return "message" in module_cfg and "period" in module_cfg

    async def run(self):
        try:
            while True:
                message = self.configuration.get("message")
                period = self.configuration.get("period")

                print(message)
                await asyncio.sleep(period)
        finally:
            print("{self.name}: requested to be stopped")

    @property
    def physical_module_dependencies(self) -> Iterable[type]:
        """No physical module needed."""
        return []
