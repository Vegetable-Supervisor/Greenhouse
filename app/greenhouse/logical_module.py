"""Greenhouse Logical Module Abstract definition."""

from abc import ABC, abstractmethod
from lib.immutable_dict import ImmutableDict
from typing import Iterable


class LogicalModule(ABC):
    """A LogicalModule is a logical module for a greenhouse.

    For example, a "temperature controler", a "watering controler".
    """

    def __init__(self, name: str, physical_modules: dict, cfg: dict = None):
        """Construct a new LogicalModule.

        Args:
            name (str): name of the module, unique in the greenhouse
            physical_modules (dict): reference to the greenhouse's loaded physical modules, indexed by name
            cfg (dict): configuration of the module
        """
        if cfg is None:
            cfg = {}

        if not self.validate_configuration(cfg):
            raise ValueError(
                f"configuration is invalid for module {name}: {cfg}")

        self.name = name
        self.physical_modules = physical_modules
        self.__configuration = ImmutableDict(cfg)

    @property
    @abstractmethod
    def physical_module_dependencies(self) -> Iterable[type]:
        """Return an iterable of physical module that this logical module depends on."""
        raise NotImplementedError

    @abstractmethod
    async def run(self) -> None:
        """Run method for the module.

        This is a coroutine that performs the job of the module.
        It is called repeateadly.
        """
        raise NotImplementedError

    @abstractmethod
    def validate_configuration(self, module_cfg: dict) -> bool:
        """Validates the configuration for the module.

        Returns True if the configuration is valid, False otherwise.
        """
        raise NotImplementedError

    def update_configuration(self, cfg: dict) -> None:
        """Updates the configuration.

        Raises a ValueError if the configuration is not valid.
        """
        if not self.validate_configuration(cfg):
            raise ValueError(
                f"configuration is invalid for module {self.name}: {cfg}")
        self.__configuration = ImmutableDict(cfg)

    @property
    def configuration(self):
        """Return the configuration of the module."""
        return self.__configuration
