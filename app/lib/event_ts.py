"""A thread safe async.Event wrapper."""

import asyncio

class EventTs(asyncio.Event):
    """Thread-safe version of asyncio.Event."""
    def set(self):
        self._loop.call_soon_threadsafe(super().set)

    def clear(self):
        self._loop.call_soon_threadsafe(super().clear)
