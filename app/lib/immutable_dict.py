"""A thread safe read-only dictionary."""
from collections.abc import Mapping
import threading


class ImmutableDict(Mapping):

    def __init__(self, *args, **kwargs):
        self.__store = dict(*args, **kwargs)
        self.__lock = threading.Lock()

    def __getitem__(self, key):
        with self.__lock:
            return self.__store[self.__keytransform__(key)]

    def __iter__(self):
        return iter(self.__store)

    def __len__(self):
        with self.__lock:
            return len(self.__store)

    def __keytransform__(self, key):
        return key
