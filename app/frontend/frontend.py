import os.path
from flask import Blueprint

STATIC_DIRECTORY = os.path.join(os.path.pardir, 'static')

frontend = Blueprint('frontend', __name__, static_folder=STATIC_DIRECTORY)


@frontend.route('/')
def index():
    return frontend.send_static_file('index.html')