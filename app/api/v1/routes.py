from flask import Blueprint
from api import common
from flask_restful import Resource, Api, reqparse
from flask_api import status
from flask.json import jsonify
from flask import current_app, request
from typing import List
import json
import logging
import importlib.util

from greenhouse.configuration import decode_configuration, validate_configuration
from api.v1.module_definition import ModuleDefinition
from greenhouse import logical_module

api_bp = Blueprint('api_v1', __name__)
api = Api(api_bp)


parser = reqparse.RequestParser()


@api_bp.route('/', methods=['GET'])
def index():
    return common.index()


class Name(Resource):
    def get(self):
        return current_app.greenhouse.name, status.HTTP_200_OK


class Configuration(Resource):
    def get(self):
        """Return the configuration of the greenhouse."""
        return current_app.greenhouse.configuration.__dict__, status.HTTP_200_OK

    def put(self):
        """Update the configuration of the greenhouse."""
        json_data = request.get_json()
        if validate_configuration(json_data):
            configuration = decode_configuration(json_data)
            # FIXME RESTFUL ?
            current_app.greenhouse.update(configuration)
        else:
            return status.HTTP_400_BAD_REQUEST


class ModuleConfiguration(Resource):
    def get(self, module_name: str):
        """Return the configuration of the module.

        Args:
            module_name (str): The name of the module.
        """
        print(module_name)
        r = current_app.greenhouse.get_module_configuration(module_name)
        if r is not None:
            return json.dumps(r), status.HTTP_200_OK
        return status.HTTP_404_NOT_FOUND

    def put(self, module_name: str):
        """Update the configuration of the module.

        Args:
            module_name (str): The name of the module.
        """
        if not request.json:
            return status.HTTP_400_BAD_REQUEST
        if "data" not in request.json:
            return status.HTTP_400_BAD_REQUEST
        cfg = request.json.get("data")
        try:
            current_app.greenhouse.set_module_configuration(
                module_name=module_name, cfg=cfg)
            logging.debug(
                f"configuration of module {module_name} changed to {current_app.greenhouse.get_module_configuration(module_name)}")
        except ValueError:
            # invalid configuration
            return None, status.HTTP_400_BAD_REQUEST
        return current_app.greenhouse.get_module_configuration(module_name), status.HTTP_200_OK

class Modules(Resource):
    def get(self):
        return list(current_app.greenhouse.get_modules_names())

    def put(self):
        if not request.json:
            return status.HTTP_400_BAD_REQUEST
        if "data" not in request.json:
            return status.HTTP_400_BAD_REQUEST
        data_modules = request.json.get("data")
        modules = []

        # FIXME: validate module list before enabling/disabling modules

        try:
            for module in data_modules:
                modules += [ModuleDefinition(**module)]
        except Exception as e:
            # FIXME
            print("exception while loading modules: " + str(e))


        gh = current_app.greenhouse
        required_modules = set([module.name for module in modules])
        loaded_modules = gh.get_modules_names()

        # disable modules
        for module_name in loaded_modules:
            if module_name not in required_modules:
                gh.unload_module(module_name)

        # enable modules
        for module in modules:
            if module.name not in loaded_modules:
                spec = importlib.util.spec_from_file_location(module.name, module.path)
                pymod = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(pymod)
                gh.load_module(module.name, getattr(pymod, module.class_name))



api.add_resource(Name, '/name')
api.add_resource(Configuration, '/configuration')
api.add_resource(ModuleConfiguration, '/module/<module_name>/configuration')
api.add_resource(Modules, '/modules')