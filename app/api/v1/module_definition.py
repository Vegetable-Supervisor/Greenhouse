"""A module definition."""
from collections import namedtuple

ModuleDefinition = namedtuple("ModuleDefinition", ['name', 'class_name', 'path'])
