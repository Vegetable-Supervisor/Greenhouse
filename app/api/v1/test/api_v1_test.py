import os
import tempfile
import unittest
from flask import Flask
from flask_restful import Api
from flask_api import status
from ast import literal_eval
from greenhouse.greenhouse import GreenHouse
from greenhouse.configuration import Configuration
from greenhouse.utils import run_core, stop_core
from api.common import index
from api.v1.routes import api_bp, api, Name, Configuration, ModuleConfiguration
from greenhouse.logical_modules.printer import Printer as PrinterModule


class PutModuleConfigurationTestCase(unittest.TestCase):

    def __init__(self, greenhouse: GreenHouse, module_name: str, cfg_in: Configuration, return_code: int = status.HTTP_200_OK):
        super().__init__()
        self.gh = greenhouse
        self.module_name = module_name
        self.cfg_in = cfg_in
        self.return_code = return_code

    def run_test(self, client):
        json_data = {
            "data": self.cfg_in
        }
        rv = client.put(f'/module/{self.module_name}/configuration',
                        json=json_data)

        # check the return code
        self.assertEqual(self.return_code, rv.status_code,
                         f"unexpected status code: expected: {self.return_code}, got: {rv.status_code}")

        if self.return_code != status.HTTP_200_OK:
            # do not check the response data
            return

        # check the content
        response = rv.data.decode("utf-8").strip().strip('"')
        response = literal_eval(response)
        self.assertEqual(self.cfg_in, response)

        # check the local configuration
        local_cfg = self.gh.get_module_configuration(self.module_name)
        self.assertEqual(self.cfg_in, local_cfg)

        return


class APIV1Test(unittest.TestCase):
    """Tests for the v1."""

    def setUp(self):
        self.gh = GreenHouse()
        self.app = Flask(__name__)
        self.app.testing = True
        self.app.greenhouse = self.gh

        self.api = Api(self.app)
        self.api.add_resource(Name, "/name")
        self.api.add_resource(Configuration, "/configuration")
        self.api.add_resource(ModuleConfiguration,
                              '/module/<module_name>/configuration')

        self.client = self.app.test_client

        self.core_stopper = run_core(self.gh)

    def tearDown(self):
        self.core_stopper.set()
        stop_core(self.gh)

    def test_get_name(self):
        rv = self.client().get('/name')
        response = rv.data.decode("utf-8").strip().strip('"')
        print(response)
        self.assertEqual(self.gh.name, response)

    def test_get_configuration(self):
        rv = self.client().get('/configuration')
        response = rv.data.decode("utf-8").strip().strip('"')
        response = literal_eval(response)
        self.assertEqual(self.gh.configuration.__dict__, response)

    def test_put_configuration(self):
        """Test for put route on a greenhouse configuration."""
        tt = [
            # normal case
            (
                {
                    "name": "gh",
                    "description": "a description",
                    "modules": {},
                },
                status.HTTP_200_OK
            ),
            # more fields than necessary
            (
                {
                    "name": "gh",
                    "description": "a description",
                    "modules": {},
                    "something": 42,
                },
                status.HTTP_200_OK,
            ),
            # invalid configuration: name is not a string
            (
                {
                    "name": 1,
                    "description": "a description",
                    "modules": {},
                },
                status.HTTP_400_BAD_REQUEST,
            ),
        ]

    def test_module_put_configuration(self):
        """Tests for put route on a module configuration."""
        loaded_module_name = "printer"
        self.gh.load_module(loaded_module_name, PrinterModule)
        tt = [
            (loaded_module_name, {"message": "hello",
                                  "period": 1}, status.HTTP_200_OK),
            (loaded_module_name, {"message": "",
                                  "period": 2}, status.HTTP_200_OK),
            (loaded_module_name, {"smth": 1}, status.HTTP_400_BAD_REQUEST)
        ]

        for (module_name, cfg_in, return_code) in tt:
            with self.subTest():
                PutModuleConfigurationTestCase(
                    self.gh, module_name, cfg_in, return_code).run_test(client = self.client())
