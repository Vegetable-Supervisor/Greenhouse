import logging
import time
import asyncio
import threading

from flask import Flask
from flask_api import status

from greenhouse.greenhouse import GreenHouse
from greenhouse.configuration import Configuration
from greenhouse.utils import run_core, stop_core
from lib.event_ts import EventTs
from lib.ssdp import SSDPServer

from frontend.frontend import frontend
from api.v1.routes import api_bp as api_v1


SERVICE_TYPE = "greenhouse"
HOST_IP = "localhost"
HOST_PORT = 5000
HTTP_URL = "https://{}:{}".format(HOST_IP, HOST_PORT)


def main():
    # setup logging
    logging.basicConfig(level=logging.DEBUG)

    # create the greenhouse

    # TODO FIXME from database / configuration file
    # if the green house restarts, configuration should be loaded from database,
    gh = GreenHouse()

    # create the SSDP server
    ssdp_server = SSDPServer()
    ssdp_server.register(
        manifestation="local",
        usn=gh.identifier,
        st=SERVICE_TYPE,
        location=HTTP_URL,
        server=gh.name,
    )

    core_stopper = run_core(gh)

    # create the flask application
    app = create_app(gh)

    # run the https server
    run_http(app)

def create_app(gh: GreenHouse) -> Flask:
    """Create the Flask application."""
    app = Flask(__name__)
    app.greenhouse = gh
    app.register_blueprint(frontend, url_prefix='/frontend')
    app.register_blueprint(api_v1, url_prefix='/api/v1')
    return app

def run_http(app: Flask) -> Flask:
    """Run the flask app as a sll http server."""
    app.run(ssl_context='adhoc')


if __name__ == "__main__":
    main()
